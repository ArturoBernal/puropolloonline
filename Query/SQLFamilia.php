<?php 
	class SQLString{

		function FamiliaDedos(){
			$SQLFamiliaDedos = "select 
	                                        a.ID_ARTICULO,
                                            a.ID_FAMILIA,
	                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
	                                        PRECIOVENTA
	                                    FROM
	                                        ARTICULO A
	                                            INNER JOIN
	                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
	                                    WHERE
	                                        a.STATUS = 1 AND APS.STATUS = 1
	                                            and A.ID_FAMILIA = '8'
	                                            And A.venta = 1
	                                            And a.precioventa != '0.00'
	                                            AND APS.ID_SUCURSAL = '4'
	                                    GROUP BY A.ID_ARTICULO
	                                    ORDER BY A.ORDEN ASC;";
	        return $SQLFamiliaDedos;
		}

		function FamiliaCombos(){

				$SQLFamiliaCombos="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '12'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";
                return $SQLFamiliaCombos;
		}

        function FamiliaBebidas(){
                $SQLFamiliaCombos="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '3'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";
                return $SQLFamiliaCombos;
        }

        function FamiliaPromociones(){
                $SQLFamiliaPromociones="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '21'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";
                return $SQLFamiliaPromociones;
        }

		function FamiliaCrusty(){
                 $SQLFamiliaCrusty="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '9'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";
                return $SQLFamiliaCrusty;
		}

		function FamiliaEnsaladas(){
            $SQLFamiliaEnsaladas="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '28'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";

            return $SQLFamiliaEnsaladas;
		}

		function FamiliaExtras(){
                $SQLFamiliaExtras="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '7'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";
                return $SQLFamiliaExtras;
		}

		function FamiliaIndividuales(){
          $SQLFamiliaIndividuales="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '6'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";
            return $SQLFamiliaIndividuales;
		}

		function FamiliaRostizado(){
			$SQLFamiliaRostizado="select 
                                        a.ID_ARTICULO,
                                        a.ID_FAMILIA,
                                        if(NOMBRE is null, 'S/NOMBRE', nombre) as nombre,
                                        PRECIOVENTA
                                    FROM
                                        ARTICULO A
                                            INNER JOIN
                                        ARTICULOPORSUCURSAL APS ON (APS.ID_ARTICULO = A.ID_ARTICULO)
                                    WHERE
                                        a.STATUS = 1 AND APS.STATUS = 1
                                            and A.ID_FAMILIA = '5'
                                            And A.venta = 1
                                            And a.precioventa != '0.00'
                                            AND APS.ID_SUCURSAL = '4'
                                    GROUP BY A.ID_ARTICULO
                                    ORDER BY A.ORDEN ASC;";
            return $SQLFamiliaRostizado;
		}

        function ArticulosPaquete($id_articulo){
                $SQLArticulosPaquete="select 
                                        a2.ID_ARTICULO, a2.NOMBRE as compuesto, ac.CANTIDAD
                                    from
                                        compuestodetalle ac
                                            inner join
                                        articulo a ON (a.id_articulo = ac.ID_ARTICULOCOMPUESTO)
                                            inner join
                                        articulo a2 ON (a2.ID_ARTICULO = ac.id_articulo)
                                            inner join
                                        familia f ON (a2.id_familia = f.id_familia)
                                    where
                                        a.id_articulo = ".$id_articulo."
                                            AND (f.extra = 1 OR f.bebida = 1)
                                            AND a.STATUS = 1
                                            AND a2.status = 1
                                            AND ac.status = 1
                                            AND a2.venta = 1;";
            return $SQLArticulosPaquete;
        }

        function ArticulosCambio($id_articulo, $id_articulopadre){
        	$SQLArticulosCambio = "select 
									    cdc.ID_ARTICULO,
									    a.NOMBRE,
									    cd.ID_ARTICULO
									from
									    compuestodetalle cd
									        inner join
									    compuestodetallecambio cdc ON (cd.id_compuestodetalle = cdc.ID_COMPUESTODETALLE)
									        inner join
									    articulo a ON (a.id_articulo = cdc.id_articulo)
									where
									    cd.id_articulo = ".$id_articulo."
									        AND CD.ID_ARTICULOCOMPUESTO = ".$id_articulopadre."
									        AND cdc.status = 1
									        AND a.venta = 1
									        AND a.status = 1;";
			return $SQLArticulosCambio;
        }

       	function ArticuloCambioOriginal($id_articulo){
       		$SQLArticulosCambioOriginal = "select 
											    ID_ARTICULO, NOMBRE, imagen, precioventa, puntos
											from
											    articulo
											where 	
											    status = 1 AND ID_ARTICULO = ".$id_articulo."
											        AND venta = 1;";
			return $SQLArticulosCambioOriginal;
       	}

        function ArticuloPromocion($id_articulo){
            $IDs = "";

            if($id_articulo != 0){
                if(count($id_articulo) > 0){
                    foreach ($id_articulo as $productos) {
                        $IDs .= $productos["id"] . ",";
                    }
                }
                $IDs = substr($IDs, 0, -1);
            }else{
                $IDs = 0;
            }

            $SQLPromocion = "select 
                                    p.id_promocion, p.nombre, p.imagen,p.precio
                                FROM
                                    promocion p
                                        inner join
                                    promocionaplica pa ON (pa.ID_PROMOCION = p.ID_PROMOCION)
                                WHERE
                                    (curdate() BETWEEN p.FECHAINICIO AND p.FECHAFIN)
                                        AND p.status = 1
                                        AND '69.00' >= p.compraminima
                                        AND if(pa.ID_ARTICULO IS NULL,
                                        1,
                                        pa.ID_ARTICULO IN (".$IDs.")) = 1
                                        and ligadoarticulo = 1
                                GROUP BY p.id_promocion;";

            return $SQLPromocion;
        }

        function IDArticuloPromocion($id_promocion){

            $SQLPromocionArticulo = "select 
                                pd.id_articulo,
                                p.nombre,
                                a.compuesto,
                                case
                                    when p.precio != 0.00 then p.precio
                                    when
                                        p.descuento != 0.00
                                    then
                                        sum(pd.cantidad * a.precioventa) - ROUND((sum(pd.cantidad * a.precioventa) * (p.descuento / 100)),
                                                2)
                                    Else sum(pd.cantidad * ifnull(p.precio, a.precioventa))
                                End as precio,
                                if(a.puntos is not null, 0, 0) as puntos,
                                p.id_promocion,
                                p.compraminima,
                                (SELECT 
                                        GROUP_CONCAT(id_articulo)
                                    FROM
                                        promocionaplica
                                    where
                                        id_promocion = ".$id_promocion.") as aplica
                            FROM
                                promocion p
                                    inner join
                                promociondetalle pd ON (pd.id_promocion = p.id_promocion)
                                    inner join
                                articulo a ON (pd.ID_ARTICULO = a.ID_ARTICULO)
                            where
                                p.status = 1 AND a.status = 1
                                    AND p.id_promocion = ".$id_promocion.";";

            return $SQLPromocionArticulo;
        }

         function InventarioSucursal($id_sucursal, $fecha){
            $SQLInventarioSucursal = "select 
                                            INF.ID_INVENTARIOFISICO,
                                            a.ID_ARTICULO,
                                            a.clave,
                                            a.nombre,
                                            inf.FISICO
                                        from
                                            inventariofisico inf
                                                inner join
                                            articulo a ON (inf.id_articulo = a.ID_ARTICULO)
                                        where
                                            date(inf.FECHAALTA) = '".$fecha."'
                                                and inf.id_sucursal = ".$id_sucursal.";";
        return $SQLInventarioSucursal;
	}
}
 ?>