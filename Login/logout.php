<?php
	include_once '../DB/Conexion.php';
   session_start();
   desconectar();

   if(session_destroy()) {
      header("Location: ../index.php");
   }
?>