﻿<!DOCTYPE html>
<html>

<head>
    <?php
        include '../../Carrito/Carrito_Class.php';
        include_once '../../DB/Conexion.php';
        include '../../Query/SQLFamilia.php';
        include '../../Login/session.php';
        $sql = new SQLString();
        conectar();
        $carrito = new Carrito();
        $carro = $carrito->get_content();

        function array_sort($array, $on, $order=SORT_ASC)
        {
            $new_array = array();
            $sortable_array = array();

            if (count($array) > 0) {
                foreach ($array as $k => $v) {
                    if (is_array($v)) {
                        foreach ($v as $k2 => $v2) {
                            if ($k2 == $on) {
                                $sortable_array[$k] = $v2;
                            }
                        }
                    } else {
                        $sortable_array[$k] = $v;
                    }
                }

                switch ($order) {
                    case SORT_ASC:
                        asort($sortable_array);
                    break;
                    case SORT_DESC:
                        arsort($sortable_array);
                    break;
                }

                foreach ($sortable_array as $k => $v) {
                    $new_array[$k] = $array[$k];
                }
            }

            return $new_array;
        }
    ?>

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>PuroPollo | Carrito de Compras</title>
    <!-- Favicon-->
    <link rel="icon" href="../../images/p.jpg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="../../plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red ls-closed">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espere un momento...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Top Bar -->
       <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a href="../../inicio.php"><img class="navbar-brand img-responsive" src="..\..\images\Logo.png"/></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user_session; ?></div>
                    <div class="email"><?php echo $nombre_sucursal; ?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">Menú de Navegación</li>
                    <li>
                        <a href="../../inicio.php">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Categorias</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="Dedos.php"><span>DEDOS</span></a>
                            </li>
                            <li>
                                <a href="Rostizado.php"><span>ROSTIZADO</span></a>
                            </li>
                            <li>
                                <a href="Crusty.php"><span>CRUSTY</span></a>
                            </li>
                            <li>
                                <a href="Combos.php"><span>COMBOS</span></a>
                            </li>
                            <li>
                                <a href="Individuales.php"><span>INDIVIDUALES</span></a>
                            </li>
                            <li>
                                <a href="Ensaladas.php"><span>ENSALADAS</span></a>
                            </li>
                            <li>
                                <a href="Extras.php"><span>EXTRAS</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>USUARIO</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#"><?php echo $user_session; ?></a>
                            </li>
                            <li>
                                <a href="../../Login/logout.php">CERRAR SESIÓN</a>
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">content_copy</i>
                            <span>PÁGINAS</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="active">
                                <a href="#">CARRITO DE COMPRAS</a>
                            </li>
                        </ul>
                    </li>
                    <li class="header">INFORMADORES</li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-red">donut_large</i>
                            <span>IMPORTANTE</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>ADVERTENCIA</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>INFORMATIVO</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">PuroPollo - Online</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="alert alert-danger text-center" style="display: none;" rol="alert" id="diverror"></div>
            </br>

			<div class="btn-group btn-group-justified" role="group" aria-label="Justified button group">
				<a href="#" class="btn bg-green waves-effect" onclick="datosTextos(<?php echo $login_session; ?>,<?php echo $id_usuariosucursal; ?>, <?php echo $id_sucursal; ?>);" role="button" data-type="success"><strong>TERMINAR VENTA</strong></a>
	            <a href="#" class="btn bg-gray waves-effect" role="button" id="totalventa"></a>
	            <a href="#" class="btn bg-gray waves-effect" role="button" id="totaltotal" style="display: none;"></a>
            </div>
            <input type="text" style="visibility: hidden;" name="" id="formapago">
            <input type="text" style="visibility: hidden;" name="" id="pago">
            </br>
	            <!-- Basic Example | Horizontal Layout -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="card">
	                        <div class="body">
	                        	<div id='wizard_horizontal'>
	                                <h2>ARTICULOS</h2>

	                                <section>
							            <div id='Tablas' class='row'>
							            <?php
 											$id_familia2 = 0;

						                    if(count($carro) > 0){
						                    $new_carro = array_sort($carro,'id_familia', SORT_DESC);
							                    foreach ($new_carro as $key) {
							                        $id_familia = $key["id_familia"];
							                        if($id_familia == 8){
							                            $imagen = "../../images/Dedos.png";
							                            $ref = "Dedos.php";
							                        }else if($id_familia == 28){
							                            $imagen = "../../images/Ensaladas.png";
							                            $ref = "Ensaladas.php";
							                        }else if($id_familia == 5){
							                            $imagen = "../../images/Rostizado.png";
							                            $ref = "Rostizado.php";
							                        }else if($id_familia == 6){
							                            $imagen = "../../images/Individuales.png";
							                            $ref = "Individuales.php";
							                        }else if($id_familia == 9){
							                            $imagen = "../../images/Crusty.png";
							                            $ref = "Crusty.php";
							                        }else if($id_familia == 12){
							                            $imagen = "../../images/Combos.png";
							                            $ref = "Combos.php";
							                        }else if($id_familia == 7){
							                            $imagen = "../../images/Extras.png";
							                            $ref = "Extras.php";
							                        }else if($id_familia == 21){
                                                        $imagen = "../../images/PROMOCIONES_PNG.png";
                                                        $ref = "Promociones.php";
                                                    }else if($id_familia == 3){
                                                        $imagen = "../../images/Bebidas.png";
                                                        $ref = "Bebidas.php";
                                                    }

							                        if($id_familia != $id_familia2){
							                		  echo "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
							                                    <div class='card'>
							                                        <div class='body'>
																		<div id='aniimated-thumbnials' class='list-unstyled row clearfix'>
							                                                <div class='col-lg-3 col-md-4 col-sm-6 col-xs-12'>
							                                                    <a href='".$ref."' data-sub-html='Demo Description'><img class='img-responsive thumbnail' src=".$imagen."></a>
							                                                </div><br/>
																				<div class='body table-responsive'>
							                                                        <table id='DatosCarrito".$id_familia."' name='tableDedos' class='table table-hover'>
							                                                			<div class='text-center'><a href='#' onclick='GetValuePromo(this.parentNode.parentNode)' class='btn bg-blue waves-effect' data-toggle='modal' data-target='#promocion' role='button'><strong>PROMOCIONES DEL PAQUETE</strong></a></div>
							                                                				</br>
							                                                            <thead>
							                                                                <tr>                                                                                
							                                                                    <th></th>                                                                                
							                                                                    <th>PRODUCTO</th>
							                                                                    <th>CANTIDAD</th>
							                                                                    <th>PRECIO</th>
							                                                                    <th>TOTAL</th>
							                                                                    <th style='display:none;'>PA</th>
							                                                                    <th style='display:none;'>ID</th>
							                                                                    <th style='display:none;'>IDPromo</th>
							                                                                </tr>
							                                                            </thead>";
							                            foreach ($new_carro as $productos) {
							                            	if($productos["id_familia"] == $id_familia){
							                                                  	  echo "<tr>                                                              
							                                                              <td class='numero'><form><input class='btn btn-danger' type=submit value='Quitar'></form></td>                                                              
							                                                              <td>".$productos["nombre"]."</td>
							                                                              <td>".$productos["cantidad"]."</td>
							                                                              <td>".$productos["precio"]."</td>
							                                                              <td>".$productos["cantidad"]*$productos["precio"]."</td>
							                                                              <td style='display:none;'>".$productos["unique_id"]."</td>
							                                                              <td style='display:none;'>".$productos["id"]."</td>
							                                                              <td style='display:none;'>0</td>
							                                                        	</tr>";
							                                    $SQLFinal = $sql->ArticulosPaquete($productos["id"]);
							                                    $result= mysql_query($SQLFinal) or die(mysql_error());
							                                	while($row=mysql_fetch_array($result)){
							                                                      echo "<tr id='$row[0]-'>                                                              
							                                                              <td><form><a href='#$row[0]' onclick='GetValue(this,this.parentNode.parentNode.parentNode.rowIndex,\"$row[1]\",".$productos["cantidad"]*$row[2].",".$productos["unique_id"].")' class='btn btn-info' data-toggle='modal'>Cambiar</a></form></td>                                                              
							                                                              <td>$row[1]</td>
							                                                              <td>".$productos["cantidad"]*$row[2]."</td>
							                                                              <td>0</td>
							                                                              <td>0</td>
							                                                              <td style='display:none;'>".$productos["unique_id"]."</td>
							                                                              <td style='display:none;'>$row[0]</td>
							                                                              <td style='display:none;'>0</td>
							                                                           </tr>"; 
							                                	}
							                               	}
							                            }
							                            					   echo "</table>
							                            					    </div>
							                            				</div>
							                            			</div>
							                            		</div>
							                            	</div>";
							                            	$id_familia2 = $id_familia;
							                        }
							                    }
							                }    

							            ?>
							            </div>
									</section>
									<h2>PAGO</h2>
									<section>
									    <h2><strong>SELECCIONE UNA FORMA DE PAGO</strong></h2>
			                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
			                                    </br>
			                                    <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">

			                                        <div class="panel panel-primary">
			                                            <div class="panel-heading" role="tab" id="headingOne_1">
			                                                <h4 class="panel-title">
			                                                    <a class="collapse" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1">
			                                                        EFECTIVO $
			                                                    </a>
			                                                </h4>
			                                            </div>
			                                            <div id="collapseOne_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_1">
			                                                <div class="panel-body">
			                                                	<div class="col-sm-2"><strong>Ingrese la cantidad $</strong></div>
																<div class="col-sm-3">
																    <input class="form-control" type="number" onkeypress="return justNumbers(event);" value="" placeholder="0.00" id="efectivo">
																</div>
			                                                </div>
			                                            </div>
			                                        </div>

			                                        <div class="panel panel-primary">
			                                            <div class="panel-heading" role="tab" id="headingTwo_1">
			                                                <h4 class="panel-title">
			                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseTwo_1" aria-expanded="false" aria-controls="collapseTwo_1">
			                                                        TARJETA
			                                                    </a>
			                                                </h4>
			                                            </div>
			                                            <div id="collapseTwo_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1">
			                                                <div class="panel-body">
																<div class="col-sm-3">
																	<div class="form-check">
																  		<input type="checkbox" class="form-check-input" id="CheckTarjeta" onchange="doalert(this.id)">
																  		<label class="form-check-label" for="CheckTarjeta">Pago con Tarjeta</label>
																	</div>
																	<input class="form-control" type="number" onkeypress="return justNumbers(event);" placeholder="0.00" value="" id="Tarjeta" disabled>
																</div>
			                                                </div>
			                                            </div>
			                                        </div>

			                                        <div class="panel panel-primary">
			                                            <div class="panel-heading" role="tab" id="headingThree_1">
			                                                <h4 class="panel-title">
			                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseThree_1" aria-expanded="false"
			                                                       aria-controls="collapseThree_1">
			                                                        EFECTIVO Y TARJETA
			                                                    </a>
			                                                </h4>
			                                            </div>
			                                            <div id="collapseThree_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_1">
			                                                <div class="panel-body">
			                                                	<div class="col-sm-3"><strong>Ingrese la cantidad en efectivo $</strong></div>
																<div class="col-sm-3">
																	<input class="form-control" type="number" onchange="Change();" placeholder="0.00" value="" id="efectivot">
																</div>
			                                                	<div class="col-sm-3"><strong>Ingrese la cantidad en tarjeta $</strong></div>
																<div class="col-sm-3">
																    <input class="form-control" type="number" placeholder="0.00" value="" id="tarjetat" disabled>
																</div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
									</section>
									<h2>CLIENTE</h2>
									<section>
			                            <div class="col-sm-3"><strong>Nombre(s)</strong></div>
										<div class="col-sm-10">
											<input class="form-control" type="text" value="" id="nombre">
										</div>
			                            <div class="col-sm-3"><strong>Apellido(s)</strong></div>
										<div class="col-sm-10">
											<input class="form-control" type="text" value="" id="apellido">
										</div>
									</section>
									<h2>DETALLES</h2>
									<section>
										<label for='comment'>OBSERVACIONES</label>
										<textarea class='form-control' rows='5' id='comment'></textarea>
									</section>
								</div>
							</div>     
						<?php
						 	if(count($carro) > 0){
						 		foreach ($new_carro as $id) {
						            $SQLFinal = $sql->ArticulosPaquete($id["id"]);
						            $result= mysql_query($SQLFinal) or die(mysql_error());
						            while($variable=mysql_fetch_array($result)) {
						                $SQLArticuloCambio = $sql->ArticulosCambio($variable[0], $id["id"]);
						                $resultCambio= mysql_query($SQLArticuloCambio) or die(mysql_error());

						              echo "<div class='modal fade' id='$variable[0]' tabindex='-1' role='dialog'>
							    				<div class='modal-dialog modal-lg' role='document'>
							        				<div class='modal-content'>
							            				<div class='modal-header'>
							                				<h4 class='modal-title' id='largeModalLabel'>A continuación se muestran el contenido del Paquete</h4>
							            				</div>
							             					<div class='modal-body'>
							                    				<input style='visibility:hidden' type='text' name='IDRow' id='IDRow' value=''/>
							                    				<input style='visibility:hidden' type='text' name='IDTabla' id='IDTabla' value=''/>
							                    				<input style='visibility:hidden' type='text' name='RowIndex' id='RowIndex' value=''/>
							                    				<input style='visibility:hidden' type='text' name='lastArt' id='lastArt' value=''/>
							                    				<input style='visibility:hidden' type='text' name='IDCantidad' id='IDCantidad' value=''/>
							                    				<input style='visibility:hidden' type='text' name='IDPadre' id='IDPadre' value=''/>                                                     
							                        			<div class='body table-responsive'>
							                            			<table name='tableDedos' class='table table-hover'>
							                                			<thead>
							                                    			<tr>
							                                        			<th style='visibility:hidden'>ID</th>
							                                        			<th style='visibility:hidden'>ID</th>
							                                        			<th>ARTICULO</th>
							                                        			<th>CANTIDAD</th>
							                                        			<th></th>
							                                    			</tr>
							                                			</thead>";
							            while($row=mysql_fetch_array($resultCambio)){
						                                          echo "<tr>
						                                                    <td style='visibility:hidden'>$row[0]</td>
						                                                    <td style='visibility:hidden'>$row[2]</td>
						                                                    <td>$row[1]</td>
						                                                    <td>1</td>
						                                                	<td><input onclick='GetValueChange(this)' class='btn btn-info' type=submit class='form-check-input' value='Cambio'></td>
						                                                </tr>";
						                }
						              	 					  echo "</table>
						                                        </div>                                                            
						                                    </div>
						                                	<div class='modal-footer'>
						                                        <button type='button' class='btn btn-link waves-effect' data-dismiss='modal'>CANCELAR</button>
						                                    </div>
						                            </div>
						                        </div>
						                    </div>";
						            }
						        }
						    }
						?>
						</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

<div class='modal fade' id='promocion' tabindex='-1' role='dialog'>
	<div class='modal-dialog modal-lg' role='document'>
		<div class='modal-content'>
			<div class='modal-header'>
				<h4 class='modal-title' id='largeModalLabel'>A continuación se muestran los articulos en promoción</h4>
			</div>
			<div class='modal-body'>
				<input style="display: none;" type='text' name='IDTablaPromo' id='IDTablaPromo' value=''/>
				<input style="display: none;" type='text' name='IDTablaPromo' id='IDArticuloPromoPadre' value=''/>                                                
			<div class='body table-responsive'>
				<table name='tableDedos' class='table table-hover'>
					<thead>
						<tr>
							<th style='visibility:hidden'>ID</th>
							<th style='visibility:hidden'>ID</th>
							<th>ARTICULO</th>
							<th>CANTIDAD</th>
							<th>PRECIO</th>
							<th style="display: none;">IDPromo</th>
							<th></th>
						</tr>
					</thead>
					<?php
						$ID_promocion = 0;

						if(count($carro) > 0){
							$IDPromocion = $sql->ArticuloPromocion($carro);

						}else{
							$IDPromocion = $sql->ArticuloPromocion(0);
						}

						$id= mysql_query($IDPromocion) or die(mysql_error());

						while($row=mysql_fetch_array($id)){
							$ID_promocion = $row[0];
						}

						if($ID_promocion != 0 && $ID_promocion != ""){
							$SQLPromocion = $sql->IDArticuloPromocion($ID_promocion);
							$ArticuloPromo = mysql_query($SQLPromocion) or die(mysql_error());

							while($row2=mysql_fetch_array($ArticuloPromo)){
								echo "<tr>";
								echo "<td style='visibility:hidden'>$row2[0]</td>
									  <td style='visibility:hidden'>$row2[0]</td>
									  <td>$row2[1]</td>
									  <td>1</td>
									  <td>$row2[3]</td>
									  <td style='display:none;'>$row2[5]</td>
									  <td><input onclick='AddPromo(this)' class='btn btn-success' type=submit class='form-check-input' value='Agregar'></td>";
								echo "</tr>";
							}
						}
					?>
				</table>
			</div>                                                            
		</div>
		<div class='modal-footer'>
			<button type='button' class='btn btn-link waves-effect' data-dismiss='modal'>CANCELAR</button>
		</div>
		</div>
	</div>
</div>
</section>

    <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="../../plugins/bootstrap-notify/bootstrap-notify.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../../plugins/node-waves/waves.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="../../plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="../../plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="../../plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>
    <script src="../../js/pages/ui/dialogs.js"></script>
    <script src="../../js/pages/ui/modals.js"></script>
    <script src="../../js/pages/forms/form-wizard.js"></script>
    
    <!-- Demo Js -->
    <script src="../../js/demo.js"></script>


    <script type="text/javascript">
        $(document).ready(function (e) {
            $('#myModal').on('show.bs.modal', function(e) {    
                var modal = $(this);
                modal.find('#texto1').val();
            });
    });
    </script>

	<script type="text/javascript">
		function Change(){
		    var current = document.getElementById("efectivot").value; 
		    document.getElementById("tarjetat").value = parseFloat(document.getElementById("totaltotal").innerHTML) - current;
		    if(parseFloat(document.getElementById("tarjetat").value) < 0){
		    	document.getElementById("tarjetat").value = "";
		    	document.getElementById("efectivot").value = "";
		    }else if(parseFloat(document.getElementById("efectivot").value) == parseFloat(document.getElementById("totaltotal").innerHTML)){
		    	document.getElementById("tarjetat").value = "";
		    	document.getElementById("efectivot").value = "";
		    }else if(document.getElementById("efectivot").value == 0){
		    	document.getElementById("tarjetat").value = "";
		    	document.getElementById("efectivot").value = "";
		    }
		}
	</script>

    <script type="text/javascript">
        function TotalVenta() {
            var IDs = [];
            var x = document.createElement("STRONG");
            var obj = document.getElementById('totalventa');
            obj.innerHTML = "";

            $("#Tablas").find("table").each(function(){ IDs.push(this.id); });
            var TotalVenta = 0;
            for(var j = 0; j < IDs.length; j++){
	            if(document.getElementById(IDs[j]) != null){
	                for (var i=0;i < document.getElementById(IDs[j]).rows.length; i++){
	                    if(document.getElementById(IDs[j]).rows[i].cells[4].innerHTML != 0 && document.getElementById(IDs[j]).rows[i].cells[4].innerHTML != "TOTAL"){
	                            TotalVenta = parseFloat(TotalVenta) + parseFloat(document.getElementById(IDs[j]).rows[i].cells[4].innerHTML);
	                    }          
	                }
	            }
            }
	                var texto = "TOTAL VENTA: $ " + TotalVenta;
	                var t = document.createTextNode(texto);
	                x.appendChild(t);
	                document.getElementById("totaltotal").innerHTML = TotalVenta;

	                obj.appendChild(x);
        }
        window.onload=TotalVenta;
    </script>

    <script type="text/javascript">
        $(function() {
                      $('input[type=submit]').click(function() {
                        var Articulos;
                        Articulos = this.value;
                        if(Articulos == "Quitar"){
                            var id_articulo = $(this).parents("tr").find("td").eq(5).html(); 
                           $.ajax({
                            data: {id:id_articulo},
                            url: '../../Carrito/Remove.php',
                            type: 'POST',
                            success: function(data) {

                            },
                            error: function(){
                            alert('Error!');
                            }
                            });
                        } 
                      });       
                });    
    </script>

	<script>
			function doalert(id){
			  if(document.getElementById(id).checked) {
			    document.getElementById("Tarjeta").value = document.getElementById("totaltotal").innerHTML;
			  }else{
			    document.getElementById("Tarjeta").value = "";
			  }
			}
	</script>

    <script type="text/javascript">
        function GetValuePromo(obj){

            var Id_Tabla = obj.children[2].id;
            var IDPadre = obj.children[2].id;
            var x = $(obj).parents("tr").find("td").eq(3).html();
            
            $(".modal-body #IDTablaPromo").val(Id_Tabla);
        }
    </script>

    <script type="text/javascript">
        function GetValue(obj, rowIndex,art,Cantidad, IDPadre){
            var id_articuloRemove = obj.getAttribute("href");
            var res = id_articuloRemove.substring(1);
            $(".modal-body #IDRow").val(res);
            while(obj.tagName.toUpperCase() !== "TABLE") {
                obj = obj.parentNode;
            }
            var Id_Tabla = obj.id;
            var y = $('#'+Id_Tabla).find('tr').eq(rowIndex).find('td').eq(2).html();
            $(".modal-body #IDTabla").val(Id_Tabla);
            $(".modal-body #RowIndex").val(rowIndex);
            $(".modal-body #lastArt").val(art);
            $(".modal-body #IDCantidad").val(y);
            $(".modal-body #IDPadre").val(IDPadre);
        }
    </script>

	<script type="text/javascript">
		function justNumbers(e)
		        {
		        var keynum = window.event ? window.event.keyCode : e.which;
		        if ((keynum == 8) || (keynum == 46))
		        return true;
		         
		        return /\d/.test(String.fromCharCode(keynum));
		        }
	</script>

    <script type="text/javascript">
        function VerOcultar(mensaje){
            document.getElementById('diverror').innerHTML = mensaje;
            $('#diverror').delay(0).show('slow');
            $('#diverror').delay(1500).hide('slow');
        }
    </script>

	<script type="text/javascript">
		function AddPromo(obj){
			var id_articulo = $(obj).parents("tr").find("td").eq(0).html();
			var id_promocion = $(obj).parents("tr").find("td").eq(5).html();
			var articulo = $(obj).parents("tr").find("td").eq(2).html();
			var cantidad = 1;
			var bandera = false;
			$(obj).parents("tr").find("td").eq(3).html();
			var IDTabla = document.getElementById("IDTablaPromo").value;
			var Index = document.getElementById(IDTabla).rows.length;
			var precio = $(obj).parents("tr").find("td").eq(4).html();
			var tt = 0;


			for (var i=0;i < document.getElementById(IDTabla).rows.length; i++){
				if(document.getElementById(IDTabla).rows[i].cells[7].innerHTML == id_promocion){
					bandera = true;
					document.getElementById(IDTabla).rows[i].cells[2].innerHTML = parseFloat(document.getElementById(IDTabla).rows[i].cells[2].innerHTML) + 1;
					tt = document.getElementById(IDTabla).rows[i].cells[2].innerHTML * precio;
					document.getElementById(IDTabla).rows[i].cells[4].innerHTML = tt;
				} 
			}

 		var  Fila = "<tr><td><form><a href='#"+id_articulo+"' onclick='RemoveFila(this,this.parentNode.parentNode.parentNode.rowIndex)' class='btn btn-danger' data-toggle='modal'>Quitar</a></form></td><td>"+articulo+"</td><td>"+cantidad+"</td><td>"+precio+"</td><td>"+parseFloat(precio * cantidad)+"</td><td style='display:none;'>"+id_articulo+"</td><td style='display:none;'>"+id_articulo+"</td><td style='display:none;'>"+id_promocion+"</td></tr>";

				if(bandera == false){
					$('#'+IDTabla+' tr:last').after(Fila);
				}
				TotalVenta();
				$('#promocion').modal('hide');
		}

	</script>

	<script type="text/javascript">
		function RemoveFila(obj, Index){
			var IDTabla = obj.parentNode.parentNode.parentNode.parentNode.parentNode.id;
			document.getElementById(IDTabla).deleteRow(Index);
			TotalVenta();
		}
	</script>

    <script type="text/javascript">
        function GetValueChange(obj){
            var i=1;
            var Index = document.getElementById("RowIndex").value;
            var Tabla = document.getElementById("IDTabla").value;
            var Fila = document.getElementById("IDRow").value;  
            var LastArticulo = document.getElementById("lastArt").value;
            var x = document.getElementById("IDCantidad").value;
            var Padre = document.getElementById("IDPadre").value;
            var Articulo = $(obj).parents("tr").find("td").eq(2).html();
            var Cantidad = $(obj).parents("tr").find("td").eq(3).html();
            var IDArticulo = $(obj).parents("tr").find("td").eq(0).html();
            var Bandera = false;
            var Bandera2 = false;
            var tama = document.getElementById(Tabla).rows.length;
            var  z = "<tr><td><form><a href='#"+IDArticulo+"' onclick='GetValue(this,this.parentNode.parentNode.parentNode.rowIndex,\""+Articulo+"\","+Cantidad+","+Padre+")' class='btn btn-info' data-toggle='modal'>Cambiar</a></form></td><td>"+Articulo+"</td><td>"+Cantidad+"</td><td>0</td><td>0</td><td style='display:none;'>"+Padre+"</td><td style='display:none;'>"+IDArticulo+"</td><td style='display:none;'>0</td></tr>";


            if(x == 1 || x == 0.5){
            	for (var i=0;i < document.getElementById(Tabla).rows.length; i++){
            		if(document.getElementById(Tabla).rows[i].cells[5].innerHTML == Padre && document.getElementById(Tabla).rows[i].cells[6].innerHTML == IDArticulo){
            			Bandera = true;
            			Bandera2 = true;
            			document.getElementById(Tabla).rows[i].cells[2].innerHTML = parseFloat(document.getElementById(Tabla).rows[i].cells[2].innerHTML) + 1;
            			document.getElementById(Tabla).deleteRow(Index);
            	}
            }

            	if(Bandera == false){
            		Bandera2 = true;
            		$('#'+Tabla).find('tr').eq(Index).replaceWith(z);
            	}

            }else{
            	for (var i=0;i < document.getElementById(Tabla).rows.length; i++){
            		if(document.getElementById(Tabla).rows[i].cells[5].innerHTML == Padre && document.getElementById(Tabla).rows[i].cells[6].innerHTML == IDArticulo){
            			Bandera2 = true;
            			document.getElementById(Tabla).rows[i].cells[2].innerHTML = parseFloat(document.getElementById(Tabla).rows[i].cells[2].innerHTML) + 1;
            			$('#'+Tabla).find('tr').eq(Index).find('td').eq(2).html(x-1);
            }
        }
    }
            
					if(Bandera2 == false){
            			$('#'+Tabla).find('tr').eq(Index).find('td').eq(2).html(x-1);
		            	$('#'+Tabla+' > tbody > tr').eq(Index - 1).after(z);
            		}

            $('#'+Fila).modal('hide');

            var MyDiv = "<div class='modal fade' id='"+IDArticulo+"' tabindex='-1' role='dialog'>"+ 
                                                                                "<div class='modal-dialog modal-lg' role='document'>"+
                                                                                    "<div class='modal-content'>"+ 
                                                                                        "<div class='modal-header'>"+
                                                                                            "<h4 class='modal-title' id='largeModalLabel'>A continuación se muestran el contenido del Paquete</h4>"+ 
                                                                                        "</div>"+
                                                                                        "<div class='modal-body'>"+
                                                                                        "<input style='visibility:hidden' type='text' name='IDRow' id='IDRow' value=''/>"+
                                                                                        "<input style='visibility:hidden' type='text' name='IDTabla' id='IDTabla' value=''/>"+
                                                                                        "<input style='visibility:hidden' type='text' name='RowIndex' id='RowIndex' value=''/>"+
                                                                                        "<input style='visibility:hidden' type='text' name='lastArt' id='lastArt' value=''/>"+
                                                                                                "<div class='body table-responsive'>"+
                                                                                                    "<table name='tableDedos' class='table table-hover'>"+
                                                                                                        "<thead>"+
                                                                                                            "<tr>"+
                                                                                                                "<th style='visibility:hidden'>ID</th>"+
                                                                                                                "<th style='visibility:hidden'>ID</th>"+
                                                                                                                "<th>ARTICULO</th>"+
                                                                                                                "<th>CANTIDAD</th>"+
                                                                                                                "<th></th>"+
                                                                                                            "</tr>"+
                                                                                                        "</thead>"+
                                                                                                          "<tr>"+
                                                                                                          "<td style='visibility:hidden'>"+Fila+"</td>"+
                                                                                                          "<td style='visibility:hidden'>"+IDArticulo+"</td>"+
                                                                                                          "<td>"+LastArticulo+"</td>"+
                                                                                                          "<td>"+Cantidad+"</td>"+
                                                                                                          "<td><input onclick='GetValueChange(this)' class='btn btn-info' type=submit class='form-check-input' value='Cambio'></td>"+
                                                                                                        "</tr>"+
                                                                                                    "</table>"+
                                                                                                "</div>"+                                                     
                                                                                        "</div>"+
                                                                                        "<div class='modal-footer'>"+
                                                                                            "<button type='button' class='btn btn-link waves-effect' data-dismiss='modal'>CANCELAR</button>"+
                                                                                        "</div>"+
                                                                                    "</div>"+
                                                                                "</div>"+
                                                                            "</div>";
        if($("#"+IDArticulo).length == 0){
            var seccion = document.getElementById("Tablas");
            seccion.insertAdjacentHTML('beforeend', MyDiv);
        }

        }
    </script>

	<script>
		var IDVenta;

		function datosTextos(iduser,idusersuc, idsucursal) {
			 var venta = [];
             var mensaje = ValidaCampos();
             var Cliente = document.getElementById('nombre').value;
             var totalventa = document.getElementById('totaltotal').innerHTML;
             var Obser = document.getElementById('comment').value;
             var formapago = document.getElementById('formapago').value;
             var pago = document.getElementById('pago').value;
             var diferencia = totalventa - pago;

           if(diferencia > 0){
                VerOcultar("No se ha cubierto el costo total de la venta");
           }else{
                if(mensaje != ""){
                    VerOcultar(mensaje);
                }else{
                    venta[0] = {
                                        idsucursal: idsucursal,
                                        idtipoventa: 1,
                                        idtipopagoventa: formapago,
                                        idclientesucursal: idsucursal,
                                        pagototal: totalventa,
                                        status: 1,
                                        efectivo: pago,
                                        idusuario: iduser,
                                        idusuariosucursal: idusersuc,
                                        Aliascliente: Cliente,
                                        Observaciones: Obser,
                                        Notebajes: 1
                                      };

                    var stringData = JSON.stringify(venta);


                    $.ajax({
                      data: {data:stringData},
                      url: "../../Query/GuardarVenta.php",
                      type: "POST",
                            success: function(data) {
                                VentaDetalle(data, idsucursal);
                                Mensaje("success");
                            },
                            error: function(){
                            alert('Error!');
                            }
                    });
                }
           }    
        }


        function ValidaCampos(){
            var mensaje = "";
            var IDs = [];
            $("#Tablas").find("table").each(function(){ IDs.push(this.id); });

            if(document.getElementById(IDs[0]) == null){
                mensaje = "No hay Articulos en el carrito";
                return mensaje;
            }

            if(document.getElementById('efectivo').value == "" || document.getElementById('efectivo').value == "0.00"){

                  if(document.getElementById('Tarjeta').value == "" || document.getElementById('Tarjeta').value == "0.00"){

                        if(document.getElementById('efectivot').value == "" || document.getElementById('efectivot').value == "0.00"){
                         
                               mensaje = "No se ha seleccionado la forma de pago";
                          
                            }else if(document.getElementById('tarjetat').value == "0.00" || document.getElementById('tarjetat').value == ""){
                                mensaje = "No se ha especificado la forma de pago";
                            }else{
                                var Tmp = parseFloat(document.getElementById('efectivot').value);
                                var Tmp2 = parseFloat(document.getElementById('tarjetat').value);
                                document.getElementById('formapago').value = 3;
                                document.getElementById('pago').value = Tmp + Tmp2;
                            }   

                      }else{
                        document.getElementById('formapago').value = 2;
                        document.getElementById('pago').value = document.getElementById('Tarjeta').value;
                      }
            }else{
                document.getElementById('formapago').value = 1;
                document.getElementById('pago').value = document.getElementById('efectivo').value;
            }

            if(document.getElementById('nombre').value == ""){
                mensaje = "Es necesario capturar el nombre del cliente";
            }

            return mensaje;
        }

		function VentaDetalle(id_venta, idsucursal){
			 var id_detalle = 0;
			 var IDs = [];
			 var ventadetalle = [];

			 $("#Tablas").find("table").each(function(){ IDs.push(this.id); });
			 	for(var j = 0; j < IDs.length; j++){
				     for (var i=0;i < document.getElementById(IDs[j]).rows.length; i++){

		                var x = document.getElementById(IDs[j]).rows[i].cells[0].innerHTML;
		                var idpromocion = document.getElementById(IDs[j]).rows[i].cells[7].innerHTML;
		                var PromocionIDArticulo = 0;
		                var bandera = x.includes("Quitar");

		                if(idpromocion == 0 || idpromocion == ""){
		                	idpromocion = 0;
		                }

		                if(idpromocion != 0 && idpromocion != "IDPromo"){
		                	PromocionIDArticulo = document.getElementById(IDs[j]).rows[i].cells[6].innerHTML;
		                }

				     	if(document.getElementById(IDs[j]).rows[i].cells[6].innerHTML != "ID" && bandera == true){
				     			var id = document.getElementById(IDs[j]).rows[i].cells[6].innerHTML;
								ventadetalle[0] = {
														id_sucursal: idsucursal,
														id_venta: id_venta,
														id_ventasucursal: idsucursal,
														id_articulo: document.getElementById(IDs[j]).rows[i].cells[6].innerHTML,
														cantidad: document.getElementById(IDs[j]).rows[i].cells[2].innerHTML,
														costo: document.getElementById(IDs[j]).rows[i].cells[3].innerHTML,
														id_promocion: idpromocion,
														id_articulopromocion: PromocionIDArticulo,
														status: 1
													  };

						     var stringDataDetalle = JSON.stringify(ventadetalle);
						     VentaDetalle = [];

							$.ajax({
							  data: {data:stringDataDetalle},
							  url: "../../Query/GuardarVentaDetalle.php",
							  async: false,
							  type: "POST",
				                    success: function(data) {
				                    	id_detalle = data;
				                    },
				                    error: function(){
				                    alert('Ha Ocurrido un error. Comunicarse al Dpto. de Sistemas');
				                    },
							});

							var ventaDetalleSub = [];
							var y = 0;

							for (var z=0;z < document.getElementById(IDs[j]).rows.length; z++){

								if(document.getElementById(IDs[j]).rows[z].cells[5].innerHTML == id && document.getElementById(IDs[j]).rows[z].cells[6].innerHTML != id){

									ventaDetalleSub[y] = {
																id_sucursal: idsucursal,
																id_ventadetalle: id_detalle,
																id_ventadetallesucursal: idsucursal,
																id_articulo: document.getElementById(IDs[j]).rows[z].cells[6].innerHTML,
																cantidad: document.getElementById(IDs[j]).rows[z].cells[2].innerHTML,
																status: 1,
																cambio: 0
									}

									y = parseFloat(y) + 1;
								}

							}

							 VentaDetalleSub = [];
						     var stringDataDetalleSub = JSON.stringify(ventaDetalleSub);

							$.ajax({
							  data: {data:stringDataDetalleSub},
							  url: "../../Query/GuardarVentaDetalleSub.php",
							  async: false,
							  type: "POST",
				                    success: function(data) {
				                    },
				                    error: function(){
				                    alert('Ha Ocurrido un error. Comunicarse al Dpto. de Sistemas');
				                    }
							});


							ventadetalle = [];
				     	}        
				     }
			 	}
		}

	</script>

	<script type="text/javascript">
		function VentaDetalleSub(idparent,idventadetalle){

			var ventaDetalleSub = [];
			var j = 0;

			for (var i=0;i < document.getElementById('DatosCarrito').rows.length; i++){

				if(document.getElementById('DatosCarrito').rows[i].cells[5].innerHTML == idparent && document.getElementById('DatosCarrito').rows[i].cells[6].innerHTML != idparent){

					ventaDetalleSub[j] = {
												id_sucursal: 9,
												id_ventadetalle: idventadetalle,
												id_ventadetallesucursal: 9,
												id_articulo: document.getElementById('DatosCarrito').rows[i].cells[6].innerHTML,
												cantidad: document.getElementById('DatosCarrito').rows[i].cells[2].innerHTML,
												status: 1,
												cambio: 0
					}

					j = parseFloat(j) + 1;
				}

			}

			 VentaDetalleSub = [];
		     var stringDataDetalleSub = JSON.stringify(ventaDetalleSub);

			$.ajax({
			  data: {data:stringDataDetalleSub},
			  url: "../../Query/GuardarVentaDetalleSub.php",
			  async: false,
			  type: "POST",
                    success: function(data) {

                    },
                    error: function(){
                    alert('Ha Ocurrido un error. Comunicarse al Dpto. de Sistemas');
                    }
			});

		}
	</script>

	<script type="text/javascript">
		function VentaTerminada(){
			$.ajax({
			  data: {data:1},
			  url: "../../Carrito/Destroy.php",
			  type: "POST",
                    success: function(data) {
                    	location.href = "../../inicio.php";
                    },
                    error: function(){
                    alert('Error!');
                    }
			});
		}

	</script>
</body>
</html>