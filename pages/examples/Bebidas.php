﻿<!DOCTYPE html>
<html>

<head>

    <?php
        include '../../Carrito/Carrito_Class.php';
        include_once '../../DB/Conexion.php';
        include '../../Query/SQLFamilia.php';
        include '../../Login/session.php';
        conectar();
        $SQL = new SQLString();
        $SQLFinal = $SQL->FamiliaBebidas();
        $carrito = new Carrito();
        $carro = $carrito->get_content();
        $Cantidad = count($carro);
        $Cantidad2 = count($carro);
        $can = 0;

            if($carro)
            {
                foreach($carro as $producto)
                {
                    $can = $can + (int)$producto["cantidad"];
                }
            }
        $Cantidad = $can;
        if($Cantidad <= 0){
            $Cantidad = 0;
        }
    ?>

<style type="text/css">
    .input-xs {
  height: 22px;
  padding: 2px 5px;
  font-size: 12px;
  line-height: 1.5; /* If Placeholder of the input is moved up, rem/modify this. */
  border-radius: 3px;
}

</style>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Bebidas | Puropollo Online</title>
    <!-- Favicon-->
    <link rel="icon" href="../../images/p.jpg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../../css/themes/all-themes.css" rel="stylesheet" />

    <link href="../../css/style2.css" rel="stylesheet" />

</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espere un momento...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
       <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a href="../../inicio.php"><img class="navbar-brand img-responsive" src="..\..\images\Logo.png"/></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Shopping-Cart -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span id="CantidadPedido" class="label-count"><?php echo $Cantidad; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">MIS COMPRAS</li>
                            <li class="body">
                                <ul class="menu">
                                    <?php
                                        if($Cantidad > 0){
                                            foreach($carro as $productos){
                                                echo "<li>
                                                            <a href='javascript:void(0);'>
                                                                <div class='icon-circle bg-cyan'>
                                                                    <i class='material-icons'>add_shopping_cart</i>
                                                                </div>
                                                                <div class='menu-info'>
                                                                    <h4>".$productos["cantidad"].' '.$productos["nombre"]."</h4>
                                                                    <p>
                                                                        <i class='material-icons'></i>Paquetes de la Familia de Dedos
                                                                    </p>
                                                                </div>
                                                            </a>
                                                        </li>";
                                            }
                                        }
                                    ?>
                                </ul>
                            </li>
                            <li class="footer" style="background-color: green">
                                <a href="CarritoCompras.php"><font color="black"><b>FINALIZAR COMPRA</b></font></a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Shopping-Cart -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user_session; ?></div>
                    <div class="email"><?php echo $nombre_sucursal; ?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
             <div class="menu">
                <ul class="list">
                    <li class="header">MENÚ DE NAVEGACIÓN</li>
                    <li class="active">
                        <a href="../../inicio.php">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Categorias</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="Dedos.php"><span>DEDOS</span></a>
                            </li>
                            <li>
                                <a href="Rostizado.php"><span>ROSTIZADO</span></a>
                            </li>
                            <li>
                                <a href="Crusty.php"><span>CRUSTY</span></a>
                            </li>
                            <li>
                                <a href="Combos.php"><span>COMBOS</span></a>
                            </li>
                            <li>
                                <a href="Individuales.php"><span>INDIVIDUALES</span></a>
                            </li>
                            <li>
                                <a href="Ensaladas.php"><span>ENSALADAS</span></a>
                            </li>
                            <li>
                                <a href="Extras.php"><span>EXTRAS</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>USUARIO</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#"><?php echo $user_session; ?></a>
                            </li>
                            <li>
                                <a href="../../Login/logout.php">CERRAR SESIÓN</a>
                            </li>
                        </ul>
                    </li>
                    <li class="header">INFORMADORES</li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-red">donut_large</i>
                            <span>IMPORTANTE</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>ADVERTENCIA</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>INFORMATIVO</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">PuroPollo - Online</a>.
                </div>
                <div class="version">
                    <b>Versión: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h1 class="text-center"><a><img class="img-responsive center-block" src="../../images/Bebidas_Leyenda.png"></a></h1>
                <br>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Bebidas
                                    <small>A Continuación se muestran las Bebidas.</small>
                                </h2>
                            </div>
                            <div class="body table-responsive">

                                <?php

                                $table = "table table-hover";
                                $row2 = "row";   

                                $result= mysql_query($SQLFinal) or die(mysql_error());
                                if(mysql_num_rows($result)==0) die("No hay registros para mostrar");

                                            echo "<table name='tableDedos' class=".$table.">
                                                <thead>
                                                    <tr id='1'>
                                                        <th>PRODUCTO</th>
                                                        <th>CANTIDAD</th>
                                                        <th>PRECIO</th>
                                                        <th></th>
                                                        <th style='display:none;'>ID</th>
                                                        <th style='display:none;'>ID_FAMILIA</th>
                                                    </tr>
                                                </thead>
                                                <tbody id='mytbody'>";

                                            if (isset($_POST['french-hens'])) {
                                                echo '<div style="border:1px solid #fc0; padding:10px;margin-top:25px;">';
                                                echo 'The values returned were:  '.implode(', ',array_values($_POST));
                                                echo '</div>';
                                            }

                                while($row=mysql_fetch_array($result))
                                {
                                                    echo "<tr id=".$row[0].">
                                                                <td> $row[2] </td>
                                                                <td><div class='numbers-row'><input type='text' name='french-hens' id='french-hens' value='1' disabled></div></td>
                                                                <td> $row[3] </td>
                                                                <td><form><input class='btn btn-success' type=submit value='Agregar'></form></td>
                                                                <td style='display:none;'> $row[0] </td>
                                                                <td style='display:none;'> $row[1] </td>
                                                            </tr>";
                                }
                                echo "</tbody>";
                                echo "</table>";

                                ?>
                                </table>

    <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>

        <script src="../../js/incrementing.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../../plugins/node-waves/waves.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>

    <!-- Demo Js -->
    <script src="../../js/demo.js"></script>

    <script type="text/javascript">
        $(function() {
              $('input[type=submit]').click(function() {
                var Articulos;
                var count = $('#mytbody').children('tr').length;

                $(this).closest('tr').find("input:text([name=french-hens])").each(function() {
                     textvalue = this.value;
                });

                Articulos = [$(this).parents("tr").find("td").eq(4).html(),$(this).parents("tr").find("td").eq(5).html(),$(this).parents("tr").find("td").eq(0).html(), textvalue, $(this).parents("tr").find("td").eq(2).html()];
                   $.ajax({
                    data: {Arreglo:Articulos},
                    url: '../../Carrito/Get.php',
                    type: 'POST',
                    success: function(data) {
                        document.getElementById("CantidadPedido").textContent= "1";
                    },
                    error: function(){
                    alert('Error!');
                    }
                    });
              });       
        });    
    </script>

</body>

</html>