﻿<!DOCTYPE html>
<html>

<head>

    <?php
        include '../Carrito/Carrito_Class.php';
        include_once '../DB/Conexion.php';
        include '../Query/SQLFamilia.php';
        include '../Login/session.php';
        conectar();
        $SQL = new SQLString();
        $carrito = new Carrito();
        $carro = $carrito->get_content();
        $Cantidad = count($carro);
        $Cantidad2 = count($carro);
        $can = 0;

            if($carro)
            {
                foreach($carro as $producto)
                {
                    $can = $can + (int)$producto["cantidad"];
                }
            }
        $Cantidad = $can;
        if($Cantidad <= 0){
            $Cantidad = 0;
        }
    ?>

<style type="text/css">
    .input-xs {
  height: 22px;
  padding: 2px 5px;
  font-size: 12px;
  line-height: 1.5; /* If Placeholder of the input is moved up, rem/modify this. */
  border-radius: 3px;
}

</style>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Auditoria | Puropollo Online</title>
    <!-- Favicon-->
    <link rel="icon" href="../images/p.jpg" type="image/x-icon">
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="../plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />

    <link href="../css/style2.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espere un momento...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
       <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a href="Auditoria.php"><img class="navbar-brand img-responsive" src="..\images\Logo.png"/></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Shopping-Cart -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span id="CantidadPedido" class="label-count"><?php echo $Cantidad; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">MIS COMPRAS</li>
                            <li class="body">
                                <ul class="menu">
                                    <?php
                                        if($Cantidad > 0){
                                            foreach($carro as $productos){
                                                echo "<li>
                                                            <a href='javascript:void(0);'>
                                                                <div class='icon-circle bg-cyan'>
                                                                    <i class='material-icons'>add_shopping_cart</i>
                                                                </div>
                                                                <div class='menu-info'>
                                                                    <h4>".$productos["cantidad"].' '.$productos["nombre"]."</h4>
                                                                    <p>
                                                                        <i class='material-icons'></i>Paquetes de la Familia de Dedos
                                                                    </p>
                                                                </div>
                                                            </a>
                                                        </li>";
                                            }
                                        }
                                    ?>
                                </ul>
                            </li>
                            <li class="footer" style="background-color: green">
                                <a href="CarritoCompras.php"><font color="black"><b>FINALIZAR COMPRA</b></font></a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Shopping-Cart -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><label id="user_session"><?php echo $user_session; ?></label></div>
                    <div class="email"><label id="nombre_sucursal"><?php echo $nombre_sucursal; ?></label><label style="display: none;" id="idsucursal"><?php echo $id_sucursal; ?></label></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
             <div class="menu">
                <ul class="list">
                    <li class="header">MENÚ DE NAVEGACIÓN</li>
                    <li class="active">
                        <a href="Auditoria.php">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Categorias</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="Auditoria.php"><span>AUDITORIA</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>USUARIO</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#"><?php echo $user_session; ?></a>
                            </li>
                            <li>
                                <a href="../Login/logout.php">CERRAR SESIÓN</a>
                            </li>
                        </ul>
                    </li>
                    <li class="header">INFORMADORES</li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-red">donut_large</i>
                            <span>IMPORTANTE</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>ADVERTENCIA</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>INFORMATIVO</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">PuroPollo - Online</a>.
                </div>
                <div class="version">
                    <b>Versión: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h1 class="text-center"><a><img class="img-responsive center-block" src="../images/DedosMenu.png"></a></h1>
                <br>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class='col-sm-4'>
                                <div class="form-group">
                                    <div class='input-group date' data-provide="datepicker" id='dtpFecha'>
                                        <input type='text' placeholder="FECHA" id='FechaI' class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <span><input class='btn btn-primary' type=submit id="buscar" value='Buscar'></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    Información de Inventario
                                    <small>Listado de articulos de Inventario.</small>
                                </h2>
                            </div>
                            <div class="body table-responsive" id="tablaDatos">
                                <table id='tablaInventario' class='table table-hover'>
                                    <thead>
                                        <tr id='1'>
                                            <th>CLAVE</th>
                                            <th>PRODUCTO</th>
                                            <th>FISICO</th>
                                            <th style='display:none;'>ID_INVENTARIOFISICO</th>
                                        </tr>
                                    </thead>
                                    <tbody id='mytbody'>
                                    </tbody>
                                </table>
                            </div>
                        <button type="button" id="GuardarInventario" class="btn btn-success btn-lg btn-block">Guardar Modificaciones de Inventario</button>
    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <script src="../js/incrementing.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <script src="../plugins/sweetalert/sweetalertInventario.min.js"></script>


    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/ui/dialogs.js"></script>
    <script src="../js/pages/ui/modals.js"></script>
  
    
    <!-- Demo Js -->
    <script src="../js/demo.js"></script>

	<script type="text/javascript">
		function justNumbers(e)
		        {
		        var keynum = window.event ? window.event.keyCode : e.which;
		        if ((keynum == 8) || (keynum == 46))
		        return true;
		         
		        return /\d/.test(String.fromCharCode(keynum));
		        }
	</script>

    <script>
      function validarSiNumero(numero){
        var value = false;

        if (/^([0-9])*$/.test(numero))
          value = true;

        return value;
      }
    </script>

    <script type="text/javascript">
        $(function () {
            $('#dtpFecha').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {
              $('input[type=submit]').click(function() {
               var fecha = $("#FechaI").val();
               var today = new Date();
               if ($('table#tablaInventario tbody tr').length > 0){
                    $("#mytbody").empty();
               }

               if(fecha==""){
                var dd = today.getDate();
                var mm = today.getMonth(); 
                var yyyy = today.getFullYear();
                if(mm<10){
                        mm='0'+mm;
                    } 
                fecha = yyyy+'-'+mm+'-'+dd;
               }

                       $.ajax({
                        data: {fechaInv:fecha,IDSucursal:2},
                        url: 'SQLInventario.php',
                        type: 'GET',
                        success: function(data) {
                             $("#mytbody").html(data);
                        },
                        error: function(){
                        alert('Error!');
                        }
                        });
              });     
        });  
    </script>

    <script type="text/javascript">
        $(function() {
              $('#GuardarInventario').click(function() {
                var IDInventarioFisico, IDArticulo, Fisico;
                var lbl = document.getElementById('user_session');
                var lbl2 = document.getElementById('idsucursal');
                nombre_usuario = lbl.innerText || lbl.textContent;
                idsucursal = lbl2.innerText || lbl2.textContent;
                $('#tablaInventario tbody tr').each(function(){
                    IDArticulo = $(this).closest('tr').attr('id');
                    $(this).children("td").each(function (index) {
                        switch(index){
                            case 3:
                                IDInventarioFisico = $(this).text();
                                break;
                            case 2:
                                Fisico = $(this).children("input").val();
                                break;
                        }
                    });
                       $.ajax({
                        data: {ID:IDInventarioFisico,Articulo:IDArticulo,Fisi:Fisico,usuario:nombre_usuario,idsucursal:idsucursal},
                        url: 'GuardarInventario.php',
                        type: 'POST',
                        success: function(data) {
                        },
                        error: function(){
                        alert('Error!');
                        }
                        });
                });
                Mensaje("successInventario");
              });     
        });  
    </script>

    <script type="text/javascript">
        $("#french-hens").on('keyup keypress',function(e) {
            var key = e.keyCode || e.which;
                if ((key == 8) || (key == 46))
                return true;
            return ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || (key >= 193 && key <= 211) || (key >= 225 && key <= 244) || key == 218 || key == 250 || key == 8 || key == 9 || key == 32) ;
        });
    </script>

</body>

</html>