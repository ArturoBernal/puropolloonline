﻿<!DOCTYPE html>
<html>

<head>

    <?php
        include 'Carrito/Carrito_Class.php';
        include_once 'DB/Conexion.php';
        include 'Query/SQLFamilia.php';
        include 'Login/session.php';
        conectar();
        $SQL = new SQLString();
        $SQLFinal = $SQL->FamiliaDedos();
        $carrito = new Carrito();
        $carro = $carrito->get_content();
        $Cantidad = count($carro);
        $Cantidad2 = count($carro);
        $can = 0;

            if($carro)
            {
                foreach($carro as $producto)
                {
                    $can = $can + (int)$producto["cantidad"];
                }
            }
        $Cantidad = $can;
        if($Cantidad <= 0){
            $Cantidad = 0;
        }
    ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Bienvenido a | Puropollo Online</title>
    <!-- Favicon-->
    <link rel="icon" href="images\p.jpg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espere un momento...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a href="inicio.php"><img class="navbar-brand img-responsive" src="images\Logo.png"/></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
				<!-- Shopping-Cart -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count"><?php echo $Cantidad; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">MIS COMPRAS</li>
                            <li class="body">
                                <ul class="menu">
                                    <?php
                                        if($Cantidad > 0){
                                            foreach($carro as $productos){
                                                echo "<li>
                                                            <a href='javascript:void(0);'>
                                                                <div class='icon-circle bg-cyan'>
                                                                    <i class='material-icons'>add_shopping_cart</i>
                                                                </div>
                                                                <div class='menu-info'>
                                                                    <h4>".$productos["cantidad"].' '.$productos["nombre"]."</h4>
                                                                    <p>
                                                                        <i class='material-icons'></i>Paquetes de la Familia de Dedos
                                                                    </p>
                                                                </div>
                                                            </a>
                                                        </li>";
                                            }
                                        }
                                    ?>
                                </ul>
                            </li>
                            <li class="footer" style="background-color: green">
                                <a href="pages/examples/CarritoCompras.php"><font color="black"><b>FINALIZAR COMPRA</b></font></a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Shopping-Cart -->

                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png"/>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user_session; ?></div>
                    <div class="email"><?php echo $nombre_sucursal; ?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENÚ DE NAVEGACIÓN</li>
                    <li class="active">
                        <a href="inicio.php">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Familia de Productos</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="pages/examples/Dedos.php"><span>DEDOS</span></a>
                            </li>
                            <li>
                                <a href="pages/examples/Rostizado.php"><span>ROSTIZADO</span></a>
                       		</li>
                            <li>
                                <a href="pages/examples/Crusty.php"><span>CRUSTY</span></a>
                       		</li>
                            <li>
                                <a href="pages/examples/Combos.php"><span>COMBOS</span></a>
                       		</li>
                            <li>
                                <a href="pages/examples/Individuales.php"><span>INDIVIDUALES</span></a>
                       		</li>
                            <li>
                                <a href="pages/examples/Ensaladas.php"><span>ENSALADAS</span></a>
                       		</li>
                            <li>
                                <a href="pages/examples/Extras.php"><span>EXTRAS</span></a>
                       		</li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Usuario</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#"><?php echo $user_session; ?></a>
                            </li>
                            <li>
                                <a href="Login/logout.php">CERRAR SESIÓN</a>
                            </li>
                        </ul>
                    </li>
                    <li class="header">INFORMADORES</li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-red">donut_large</i>
                            <span>IMPORTANTE</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>ADVERTENCIA</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>INFORMATIVO</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="http://www.puropollo.com.mx">PuroPollo - Online</a>.
                </div>
                <div class="version">
                    <b>Versión: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h1 class="text-center"><a><img class="img-responsive center-block" src="images/Menu.png"></a></h1>
                <br>
            </div>

            <!-- Widgets -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Listado de Categorias
                                <small>Todas las imágenes fueron tomadas de <a href="http://www.puropollo.com.mx/" target="_blank">puropollo.com.mx</a></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Dedos.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Dedos.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Rostizado.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Rostizado.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Crusty.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Crusty.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Combos.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Combos.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Individuales.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Individuales.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Ensaladas.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Ensaladas.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Extras.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Extras.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Promociones.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/PROMOCIONES_PNG.png">
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="pages/examples/Bebidas.php" data-sub-html="Demo Description">
                                        <img class="img-responsive thumbnail" src="images/Bebidas.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="plugins/raphael/raphael.min.js"></script>
    <script src="plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="plugins/flot-charts/jquery.flot.js"></script>
    <script src="plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
</body>

</html>